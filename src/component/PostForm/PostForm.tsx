import { FormEvent, useState } from "react";
import "./PostForm.css"
interface IPostForm {
    onClickPost: (topic: String, content: String) => void
}
const PostForm = ({ onClickPost }: IPostForm) => {
    const onClickSubmit = (e: FormEvent) => {
        onClickPost(topic, content)
    }
    const [topic, setTopic] = useState<string>("")
    const [content, setContent] = useState<string>("")
    const onChangeTopic = (e: FormEvent) => {
        const target = e.target as HTMLInputElement;
        setTopic(target.value);
    }
    const onChangeContent = (e: FormEvent) => {
        const target = e.target as HTMLInputElement;
        setContent(target.value);
    }
    return <div className="post-form">
        <input type="text" placeholder="Input topic" onChange={onChangeTopic} value={topic} maxLength={50} />
        <textarea className="post-input" placeholder="Input content" value={content} onChange={onChangeContent} maxLength={280} />
        <button className="post-button" onClick={onClickSubmit}>Post</button>
    </div>
}

export default PostForm;