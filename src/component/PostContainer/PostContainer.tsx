import { PublicKey } from "@solana/web3.js";
import idl from "../../kantinsmart.json";
import './PostContainer.css'

interface IPostContainer {
    author: PublicKey,
    timestamp: BigInt64Array,
    topic: String,
    content: String
}

const PostContainer = ({ author, timestamp, topic, content }: IPostContainer) => {
    return <div className="post-container">
        <h1 className="post-title">{topic}</h1>
        <p className="post-content">{content}</p>
        <span className="post-author">Published by : {author.toBase58()}</span>
    </div>
}
export default PostContainer;