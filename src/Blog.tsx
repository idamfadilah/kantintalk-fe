import { AnchorProvider, Program, web3 } from "@project-serum/anchor";
import { useConnection, useWallet } from "@solana/wallet-adapter-react";
import { PublicKey } from "@solana/web3.js";
import { useEffect, useState } from "react";
import PostContainer from "./component/PostContainer/PostContainer";
import PostForm from "./component/PostForm/PostForm";
import idl from "./kantinsmart.json";

const PROGRAM_KEY = new PublicKey(idl.metadata.address);

const Blog = () => {
    const { publicKey, wallet, signTransaction, signAllTransactions } = useWallet();
    const { connection } = useConnection();
    const idls: any = idl
    connection.onAccountChange(
        PROGRAM_KEY,
        (updatedAccountInfo, context) =>
            console.log("Updated account info: ", updatedAccountInfo),
        "confirmed"
    );
    const [posts, setPosts] = useState<any>();
    useEffect(() => {
        const fetch = async () => {
            if (!wallet || !publicKey || !signTransaction || !signAllTransactions) {
                return;
            }
            const signerWallet = {
                publicKey: publicKey,
                signTransaction: signTransaction,
                signAllTransactions: signAllTransactions,
            };
            const provider = await new AnchorProvider(connection, signerWallet, {});
            const program = await new Program(idls, PROGRAM_KEY, provider);
            setPosts(await program.account.post.all())
        }
        fetch()
    }, [])

    useEffect(() => {
        const fetch = async () => {
            if (!wallet || !publicKey || !signTransaction || !signAllTransactions) {
                return;
            }
            const signerWallet = {
                publicKey: publicKey,
                signTransaction: signTransaction,
                signAllTransactions: signAllTransactions,
            };
            const provider = await new AnchorProvider(connection, signerWallet, {});
            const program = await new Program(idls, PROGRAM_KEY, provider);
            console.log(await program.account.post.all())
            setPosts(await program.account.post.all())
        }
        fetch()
    }, [publicKey, wallet])


    const onClickSend = async (topic: String, content: String) => {
        console.log(publicKey)
        if (!wallet || !publicKey || !signTransaction || !signAllTransactions) {
            return;
        }
        const signerWallet = {
            publicKey: publicKey,
            signTransaction: signTransaction,
            signAllTransactions: signAllTransactions,
        };
        const post = web3.Keypair.generate();
        const provider = await new AnchorProvider(connection, signerWallet, {});
        const program = await new Program(idls, PROGRAM_KEY, provider);
        const pro = await program.rpc.sendPost(topic, content, {
            accounts: {
                post: post.publicKey,
                author: signerWallet.publicKey,
                systemProgram: web3.SystemProgram.programId,
            },
            signers: [post],
        });
        console.log(pro)
        console.log(program)
        console.log(provider)
        setPosts(await program.account.post.all())


    }

    return <>
        <PostForm onClickPost={onClickSend} />
        {posts?.map((value: any) => {
            return <>
                <PostContainer key={value.publicKey.toBase58()} author={value.account.author} timestamp={value.account.timestamp} topic={value.account.topic} content={value.account.content} />
            </>
        })}
    </>
}
export default Blog;