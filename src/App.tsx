import React, { FC, useEffect, useMemo } from 'react';
import { ConnectionProvider, useWallet, WalletProvider } from '@solana/wallet-adapter-react';
import { WalletAdapterNetwork } from '@solana/wallet-adapter-base';
import {
  GlowWalletAdapter,
  PhantomWalletAdapter,
  SlopeWalletAdapter,
  SolflareWalletAdapter,
  SolletExtensionWalletAdapter,
  SolletWalletAdapter,
  TorusWalletAdapter,
} from '@solana/wallet-adapter-wallets';
import {
  WalletModalProvider,
  WalletDisconnectButton,
  WalletMultiButton
} from '@solana/wallet-adapter-react-ui';
import { clusterApiUrl, PublicKey } from '@solana/web3.js';
import idl from "./kantinsmart.json";
import { Program, AnchorProvider } from '@project-serum/anchor';

import {
  useAnchorWallet,
  useConnection
} from "@solana/wallet-adapter-react";
import Blog from './Blog';
// Default styles that can be overridden by your app
require('@solana/wallet-adapter-react-ui/styles.css');



function App() {
  // const wallet = useAnchorWallet();


  // const wallet = {
  //   signTransaction(tx: Transaction): signTransaction,
  //   signAllTransactions(txs: Transaction[]): signAllTransactions,
  //   publicKey: publicKey,
  // }
  // console.log(wallet)
  // The network can be set to 'devnet', 'testnet', or 'mainnet-beta'.
  const PROGRAM_KEY = new PublicKey(idl.metadata.address);
  const network = WalletAdapterNetwork.Devnet;
  // You can also provide a custom RPC endpoint.
  const endPoint = "http://127.0.0.1:8899";
  console.log(PROGRAM_KEY)

  // const program = new Program(idl, PROGRAM_KEY, provoder);

  // @solana/wallet-adapter-wallets includes all the adapters but supports tree shaking and lazy loading --
  // Only the wallets you configure here will be compiled into your application, and only the dependencies
  // of wallets that your users connect to will be loaded.
  const wallets = useMemo(
    () => [
      new PhantomWalletAdapter(),
      new GlowWalletAdapter(),
      new SlopeWalletAdapter(),
      new SolflareWalletAdapter({ network }),
      new TorusWalletAdapter(),
    ],
    [network]
  );

  return (
    <ConnectionProvider endpoint={endPoint}>
      <WalletProvider wallets={wallets}>
        <WalletModalProvider>
          <WalletMultiButton />
          { /* Your app's components go here, nested within the context providers. */}
          <Blog />
        </WalletModalProvider>
      </WalletProvider>
    </ConnectionProvider>
  );
}

export default App;
